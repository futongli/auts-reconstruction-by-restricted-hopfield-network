import numpy as np

from components.data_am2 import data, desired
from components.RHN import RHN
from components.Optimizer import SPSA
from components.Helper import display_image, apply_distortion


hidden_num = 10

memory = np.asarray(desired).T
'''print("***********************Original Image************************")
for i in range (len(data)):
    display_image(data[i], 7, 5)'''

distort = apply_distortion(np.asarray(desired), 5)
# np.save("./components/distort", distort)
# distort = np.load('./components/distort.npy')
test = np.asarray(distort).T
print("***********************Distorted Image************************")
for i in range (len(distort)):
    display_image(distort[i], 7, 5)

test_iter = 1
for i in range(test_iter):
    # Create the optimizer & RHN
    opt_iter = 10000
    optimizer = SPSA(a=100.0, c=2.0, A=opt_iter/10.0, alpha=1.0, gamma=0.167)
    rhn = RHN(hidden_num=hidden_num, feedback_num=2, memory=memory, optimizer=optimizer, num_iterations=opt_iter)
    rhn.RHN_model()
    pred_test = rhn.predict(test).T
    print("***********************Recreated Image************************")
    for i in range (len(pred_test)):
        display_image(pred_test[i], 7, 5)

